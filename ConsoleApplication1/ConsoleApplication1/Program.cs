﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(120, 40);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Title = Properties.Resources.ConsoleTitle;
            var rooms = new ManagerClass();
            rooms.DoWork();
            Console.Read();
        }
    }
}
