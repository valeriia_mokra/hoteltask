﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GroupApp.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GroupApp.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.Make reservation, 2.Weed, 3.Drop
        ///Action #: .
        /// </summary>
        internal static string Actions {
            get {
                return ResourceManager.GetString("Actions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Now you are in admin mode. Please, select action number.
        ///1. Show reservations
        ///2. Show reservated rooms by dates
        ///3. Delete reservation
        ///4. Add room
        ///5. Remove room
        ///6. Exit
        ///
        ///Action: .
        /// </summary>
        internal static string adminMode {
            get {
                return ResourceManager.GetString("adminMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to admin.
        /// </summary>
        internal static string AdminName {
            get {
                return ResourceManager.GetString("AdminName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1234.
        /// </summary>
        internal static string AdminPhone {
            get {
                return ResourceManager.GetString("AdminPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter reservation index: .
        /// </summary>
        internal static string askDeleteReservationIndex {
            get {
                return ResourceManager.GetString("askDeleteReservationIndex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the end date: .
        /// </summary>
        internal static string askEndDate {
            get {
                return ResourceManager.GetString("askEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter initial date: .
        /// </summary>
        internal static string askFirstDate {
            get {
                return ResourceManager.GetString("askFirstDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the desired maximal cost:.
        /// </summary>
        internal static string askMaxRoomCost {
            get {
                return ResourceManager.GetString("askMaxRoomCost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the desired minimal cost:.
        /// </summary>
        internal static string askMinRoomCost {
            get {
                return ResourceManager.GetString("askMinRoomCost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter room parameters.
        /// </summary>
        internal static string askRoomAdd {
            get {
                return ResourceManager.GetString("askRoomAdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter room cost: .
        /// </summary>
        internal static string askRoomCost {
            get {
                return ResourceManager.GetString("askRoomCost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter room number which you want delete: .
        /// </summary>
        internal static string askRoomDelete {
            get {
                return ResourceManager.GetString("askRoomDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter room number: .
        /// </summary>
        internal static string askRoomID {
            get {
                return ResourceManager.GetString("askRoomID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter room type: .
        /// </summary>
        internal static string askRoomType {
            get {
                return ResourceManager.GetString("askRoomType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter secret word that you type when you made reservation.
        /// </summary>
        internal static string AskSecretWord {
            get {
                return ResourceManager.GetString("AskSecretWord", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancaled.
        /// </summary>
        internal static string Cancaled {
            get {
                return ResourceManager.GetString("Cancaled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want make reservation in this room?
        ///1 - YES
        ///2 - NO.
        /// </summary>
        internal static string Confirm {
            get {
                return ResourceManager.GetString("Confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RAINBOW HOTEL.
        /// </summary>
        internal static string ConsoleTitle {
            get {
                return ResourceManager.GetString("ConsoleTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By the: 1. cost, 2. type, 3. floor.
        /// </summary>
        internal static string Criterions {
            get {
                return ResourceManager.GetString("Criterions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your name:.
        /// </summary>
        internal static string enterName {
            get {
                return ResourceManager.GetString("enterName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type your phone number, type only numbers without symbols.
        /// </summary>
        internal static string EnterPhone {
            get {
                return ResourceManager.GetString("EnterPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter secret word .
        /// </summary>
        internal static string EnterSecretWord {
            get {
                return ResourceManager.GetString("EnterSecretWord", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You incorrectly typed the values..
        /// </summary>
        internal static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You incorrectly typed the range, or the range of no results.
        ///Please try one more time..
        /// </summary>
        internal static string ErrorDate {
            get {
                return ResourceManager.GetString("ErrorDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Find XML file with info or call specialist!!!.
        /// </summary>
        internal static string ExptXML {
            get {
                return ResourceManager.GetString("ExptXML", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        /// </summary>
        internal static System.Drawing.Icon Gnome {
            get {
                object obj = ResourceManager.GetObject("Gnome", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to                                          ******** H O T E L ********
        ///.
        /// </summary>
        internal static string HotelInformation {
            get {
                return ResourceManager.GetString("HotelInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You enter incorrect Name. Please try again.
        /// </summary>
        internal static string IncorrectName {
            get {
                return ResourceManager.GetString("IncorrectName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You enter incorrect Phone Number. Do you want try again?  
        ///Enter Yes or No.
        /// </summary>
        internal static string IncorrectPhone {
            get {
                return ResourceManager.GetString("IncorrectPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter number of the floor:.
        /// </summary>
        internal static string numberOfFloor {
            get {
                return ResourceManager.GetString("numberOfFloor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter number of the room for reservation:.
        /// </summary>
        internal static string numberOfRoom {
            get {
                return ResourceManager.GetString("numberOfRoom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to do a new reservation? 1. Yes, 2. No.
        /// </summary>
        internal static string repeat {
            get {
                return ResourceManager.GetString("repeat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dates: {0} - {1}
        ///Cost: {2}
        ///Number: {3}
        ///Floor: {4}.
        /// </summary>
        internal static string ReservationInfo {
            get {
                return ResourceManager.GetString("ReservationInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1. Delete reservation, 2. Edit reservation, 3. Add reservation.
        /// </summary>
        internal static string secondModeMenu {
            get {
                return ResourceManager.GetString("secondModeMenu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please type your Full Name.
        /// </summary>
        internal static string TypeName {
            get {
                return ResourceManager.GetString("TypeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter type of the room:.
        /// </summary>
        internal static string typeOfRoom {
            get {
                return ResourceManager.GetString("typeOfRoom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We find reservation on you name.
        ///If you want see them or change  type your phone number, type only numbers without symbols.
        /// </summary>
        internal static string TypePhone {
            get {
                return ResourceManager.GetString("TypePhone", resourceCulture);
            }
        }
    }
}
