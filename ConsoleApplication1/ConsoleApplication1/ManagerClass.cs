﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using GroupApp.Properties;

namespace GroupApp
{
    class ManagerClass
    {

        private RoomsList _roomsCatalog = new RoomsList();

        private RoomsList _tempList;

        InputOutput InOut = new InputOutput();

        Reservation reservation;

        public void LoadDataFromFile()
        {

                _roomsCatalog = new MySerialization<RoomsList>().Deserialize();
                _roomsCatalog.CleanOldReservation();
                SaveDataToFile();
        }

        public void ShowTempList()
        {
            Console.WriteLine(_tempList.ToString());
        }
        

        public void FirstVisitMode()
        {
            while (true)
            {
                Console.Clear();
                bool Reapeat = true;
                while (true)
                {
                    try
                    {
                        var dates = InOut.AskDates();
                        _tempList = _roomsCatalog.FindDate(dates[0], dates[1], true);
                        if (_tempList == null || _tempList.Count() == 0 || dates[0] >= dates[1] ||
                            dates[1] < DateTime.Now || dates[0] < DateTime.Today)
                        {
                            Console.Clear();
                            Console.WriteLine(Resources.ErrorDate);
                        }
                        else
                        {
                            Console.Clear();
                            ShowTempList();
                            reservation.From = dates[0];
                            reservation.To = dates[1];
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Clear();
                        Console.WriteLine(e.Message);
                        Console.WriteLine(Resources.ErrorDate);
                    }
                }

                while (true)
                {
                    bool cancel = false;
                    try
                    {
                        int numberOfActions = InOut.AskAction();
                        switch (numberOfActions)
                        {
                            case 1:
                                InOut.AskReservation(reservation, _roomsCatalog, _tempList);
                                SaveDataToFile();
                                Reapeat = InOut.AskRepeat(Resources.repeat);
                                cancel = true;
                                break;
                            case 2:
                                Weed(InOut.AskCriterion());
                                ShowTempList();
                                break;
                            case 3:
                                cancel = true;
                                break;
                            default:
                                throw new ArgumentException();
                        }
                    }
                    catch(ArgumentException e)
                    {
                        Console.Clear();
                        Console.WriteLine(e.Message);
                        ShowTempList();
                    }

                    if (!Reapeat || cancel) break;
                }

                if (!Reapeat) break;
            }

        }

        public void AdminMode()
        {
            string messageOnRedraw = "";
            bool exit = false;

            while(true)
            {
                Console.Clear();
                Console.WriteLine(messageOnRedraw);
                messageOnRedraw = "";

                Console.Write(Resources.adminMode);
                try
                { 
                    switch (Console.ReadLine()[0])
                    {
                        case '1':
                            messageOnRedraw = InOut.ShowAllReservations(_roomsCatalog);
                            break;
                        case '2':
                            Console.Clear();
                            DateTime[] tempDates = InOut.AskDates();
                            messageOnRedraw = _roomsCatalog.FindDate(tempDates[0], tempDates[1], false).ToString();
                            break;
                        case '3':
                            InOut.askDeleteReservation(_roomsCatalog);
                            SaveDataToFile();
                            break;
                        case '4':
                            Console.Clear();
                            _roomsCatalog.Add(InOut.AskRoomAdd());
                            SaveDataToFile();
                            break;
                        case '5':
                            _roomsCatalog.Delete(InOut.AskRoomDelete(_roomsCatalog));
                            SaveDataToFile();
                            break;
                        case '6':
                            exit = true;
                            break;
                        default:
                            break;
                    }
                }

                // Catch block

                catch (FormatException e)
                {
                    messageOnRedraw = "Invalid format\n";
                }
                catch (IndexOutOfRangeException e)
                {
                    messageOnRedraw = "Invalid index\n";
                }


                if (exit == true)
                    break;
            }
        }

        public void SecondVisitMode(string name)
        {
            bool exit = false;
            Console.Clear();
            while (true)
            {
                try
                {
                    Console.WriteLine(InOut.ShowAllReservations(_tempList));
                    Console.WriteLine(Resources.secondModeMenu);
                    switch (Console.ReadKey().KeyChar)
                    {
                        case '1':
                            InOut.askDeleteReservation(_roomsCatalog,_tempList);  
                            SaveDataToFile();
                            if (_tempList.FindNumbersWithReservedOnName(name).Count() < 1) exit = true;                          
                            break;
                        case '2':
                            break;
                        case '3':
                            FirstVisitMode();
                            break;
                        case '4':
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("\n"+Resources.Error);
                            Console.ReadKey();
                            break;
                    }
                    Console.Clear();
                }
                catch(Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
                if (exit) break;
                
            }
        }

        public void DoWork()
        {
          
            LoadDataFromFile();
            while (true)
            {
                try
                {
                    Console.WriteLine(Resources.HotelInformation);
              //  ShowList();

                    var name = InOut.AskName();
                    if (name.Equals(Resources.AdminName))
                    {
                        if (InOut.AskPhone(Resources.TypePhone).Equals(Resources.AdminPhone))
                            AdminMode();
                    }
                    else
                    {
                        _tempList = _roomsCatalog.FindNumbersWithReservedOnName(name);
                        if (_tempList.Count() == 0)
                        {
                            reservation = new Reservation(name);
                            FirstVisitMode();
                        }
                        else
                        {
                            while (true)
                            {
                                var phone = InOut.AskPhone(Resources.TypePhone);
                                var temp = _tempList.FindNumbersWithReservedOnPhone(phone);
                                if (temp.Count() == 0)
                                {
                                    if (!InOut.AskRepeat(Resources.IncorrectPhone))
                                    {
                                        reservation = new Reservation(name);
                                        FirstVisitMode();
                                        break;
                                    }
                                    Console.Clear();
                                }
                                else
                                {
                                    _tempList = temp;
                                    reservation = new Reservation(name, phone);
                                    SecondVisitMode(name);
                                    break;
                                }
                            }                            
                        }
                    }
                    Console.Clear();

                }
                catch (ArgumentException e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message + "\n" + Resources.ExptXML);
                }
            }

            // Catch block

           
        }

        private void Weed(int numberOfAction)
        {
            var temp = new RoomsList();
            switch (numberOfAction)
            {
                case 1:
                    temp = InOut.AskRoomCost(_tempList);
                    break;
                case 2:
                    temp = InOut.AskRoomType(_tempList);
                    break;
                case 3:
                    temp = InOut.AskRoomFloor(_tempList);
                    break;
                default:
                    throw new ArgumentException();
            }
            if (temp != null && temp.Count() != 0)
                _tempList = temp;
            else throw new ArgumentException();
        }


        public void SaveDataToFile()
        {
            MySerialization<RoomsList>d = new MySerialization<RoomsList>();
            d.Serialize(_roomsCatalog);
        }

        public void ShowList()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(_roomsCatalog.ToString());
            Console.ForegroundColor = ConsoleColor.Green;
        }
    }
}
