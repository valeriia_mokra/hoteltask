﻿using System;
using BusinessLogic;
using GroupApp.Properties;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace GroupApp
{
    class InputOutput 
    {
        public int AskAction()
        {
            Console.WriteLine(Resources.Actions);
            return Convert.ToInt16(Console.ReadLine());
        }

        public int AskCriterion()
        {
            Console.WriteLine(Resources.Criterions);
            return Convert.ToInt16(Console.ReadLine());
        }

        public DateTime[] AskDates()
        {
            DateTime[] result = new DateTime[2];

            Console.WriteLine(Resources.askFirstDate);
            result[0] = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine(Resources.askEndDate);
            result[1] = Convert.ToDateTime(Console.ReadLine());

            return result;
        }


        public bool AskRepeat(string message)
        {
            Console.WriteLine(message);
            switch (Console.ReadLine().ToLower())
            {
                case "1":
                case "y":
                case "yes":
                    return true;
                default:
                    return false;;
            }
        }

        public void AskReservation(Reservation reservationWithName, RoomsList roomList, RoomsList tempList)
        {
            Console.WriteLine(Resources.numberOfRoom);
            var numberRoom = Console.ReadLine();
            var phone = AskPhone(Resources.EnterPhone);
            Console.WriteLine(Resources.EnterSecretWord);
            var secretWord = Console.ReadLine();
            reservationWithName.MobileNumber = phone;
            reservationWithName.SecretWord = secretWord;

            if (tempList.Contains(roomList.FindRoom(numberRoom)))
            {
                Console.Clear();
                HotelRoom temp = roomList.FindRoom(numberRoom);
                Console.WriteLine(Resources.ReservationInfo, reservationWithName.From.ToString(),
                    reservationWithName.To.ToString(), roomList.GetAllCost(reservationWithName, temp.Cost),
                    temp.ID, temp.ID[2]);
                Console.WriteLine(Resources.Confirm);
                switch (Console.ReadLine().ToLower())
                {
                    case "yes":
                    case "y":                    
                    case "1":
                        roomList.AddReservation(reservationWithName, numberRoom);
                        break;
                    case "no":
                    case "n":
                    case "2":
                        Console.WriteLine(Resources.Cancaled);
                        break;
                    default:
                        throw new ArgumentException();
                }
            }
            else throw new ArgumentException();
        }

        public RoomsList AskRoomCost(RoomsList roomList)
        {
            Console.WriteLine(Resources.askMinRoomCost);
            var minCost = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(Resources.askMaxRoomCost);
            var maxCost = Convert.ToDouble(Console.ReadLine());

            return roomList.FindCoast(minCost, maxCost);
        }

        public RoomsList AskRoomFloor(RoomsList roomList)
        {
            Console.WriteLine(Resources.numberOfFloor);
            var numberFloor = Convert.ToChar(Console.ReadLine());

            return roomList.FindFloor(numberFloor);
        }

        public RoomsList AskRoomType(RoomsList roomList)
        {
            Console.WriteLine(Resources.typeOfRoom);
            var stringTypeRoom = Console.ReadLine();

            foreach (var name in Enum.GetNames(typeof(RoomTypes)))
            {
                Console.Write("{0:D}.{1} ",
                           Enum.Parse(typeof(RoomTypes), name),
                           name);
            }
            Console.Write("\n");

            var typeRoom = (RoomTypes)Enum.Parse(typeof(RoomTypes), stringTypeRoom);
            return roomList.FindType(typeRoom);
        }

        public String AskName()
        {
            Console.WriteLine(Resources.TypeName);
            string name = Console.ReadLine();
            if (new Regex(@"[^a-zA-Z ]").IsMatch(name)||String.IsNullOrEmpty( name))
                throw new ArgumentException(Resources.IncorrectName);
            else return name;
        }

        public String AskPhone(string message)
        {
            Console.WriteLine(message);
            string phone = Console.ReadLine();

            if (new Regex(@"[^0-9 ]").IsMatch(phone))
                throw new ArgumentException(Resources.IncorrectPhone);
            else return phone;
        }

        public HotelRoom AskRoomAdd()
        {
            Console.WriteLine(Resources.askRoomAdd);
            Console.Write(Resources.askRoomID);
            string ID = Console.ReadLine();
            Console.Write(Resources.askRoomCost);
            string cost = Console.ReadLine();
            Console.Write(Resources.askRoomType);
            RoomTypes roomtype = (RoomTypes)Enum.Parse(typeof(RoomTypes), Console.ReadLine());

            return new HotelRoom(ID, Convert.ToDouble(cost), roomtype);
        }

        public HotelRoom AskRoomDelete(RoomsList roomsList)
        {
            Console.Clear();
            Console.WriteLine(roomsList.ToString());
            Console.Write(Resources.askRoomDelete);
            return roomsList.FindRoom(Console.ReadLine());
        }

        public string ShowAllReservations(RoomsList roomsList)
        {
            string result = "";

            foreach(HotelRoom room in roomsList.AllReservations())
            {
                result += "\n" + room.ToString();
                for(int i = 0; i < room.Reservations.Count; i++)
                {
                    result += "\tReservation №" + (i+1).ToString() + room.Reservations[i].ToString() + "\n";
                }
            }

            return result;
        }

        public void askDeleteReservation(RoomsList roomList)
        {
            Console.Clear();
            Console.WriteLine(ShowAllReservations(roomList));

            Console.Write(Resources.askRoomID);
            string ID = Console.ReadLine();
            Console.Write(Resources.askDeleteReservationIndex);
            string index = Console.ReadLine();
            
            roomList.DeleteReservation(ID, Convert.ToInt16(index)-1);
        }

        public void askDeleteReservation(RoomsList roomList, RoomsList temp_roomList)
        {           

            Console.Write(Resources.askRoomID);
            string ID = Console.ReadLine();
            if (!(new Regex(@"^\d{4}$").IsMatch(ID))||!temp_roomList.Contains(ID))
                throw new ArgumentException();

            Console.Write(Resources.askDeleteReservationIndex);
            int index = Convert.ToInt16(Console.ReadLine());
            HotelRoom tempRoom = temp_roomList.FindRoom(ID); //temp room

            if (tempRoom.Reservations.Count<Convert.ToInt16(index))
                throw new ArgumentException();

            Reservation tempReservation = tempRoom.Reservations[index - 1];
            Console.Write(Resources.AskSecretWord);
            string secretWord = Console.ReadLine();
            if(secretWord.Equals(tempReservation.SecretWord))
            roomList.DeleteReservation(ID, tempRoom.Reservations[index-1]);
            else throw new ArgumentException();
        }
    }
}
