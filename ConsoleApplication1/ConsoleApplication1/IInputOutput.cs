﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace GroupApp
{
    interface IInputOutput
    {
        DateTime[] AskDates();
        int AskAction();
        int AskCriterion();
        bool AskRepeat();
        void AskReservation(Reservation reservationWithoutName, RoomsList roomList, RoomsList tempList);
        RoomsList AskRoomCost(RoomsList roomList);
        RoomsList AskRoomType(RoomsList roomList);
        RoomsList AskRoomFloor(RoomsList roomList);
    }
}
