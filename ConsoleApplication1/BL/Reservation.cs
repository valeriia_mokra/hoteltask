﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public struct Reservation
    {
        public DateTime From;
        public DateTime To;
        public string Name;
        public string MobileNumber;
        public string SecretWord;

        public Reservation(DateTime From, DateTime To, string Name, string MobileNumber, string SecretWord)
        {
            this.From = From;
            this.To = To;
            this.Name = Name;
            this.MobileNumber = MobileNumber;
            this.SecretWord = SecretWord;
        }

        public Reservation(DateTime From, DateTime To)
        {
            this.From = From;
            this.To = To;
            this.Name = "";
            this.MobileNumber = "";
            this.SecretWord = "";
        }

        public Reservation(string Name, string MobileNumber="")
        {
            this.From = new DateTime();
            this.To = new DateTime();
            this.Name = Name;
            this.MobileNumber = MobileNumber;
            this.SecretWord = "";
        }

        public override string ToString()
        {
            return String.Format("\tDates: {0} - {1}\n\tName: {2}\n\tMobileNumber: {3}\n",
                From, To, Name, MobileNumber);
        }
    }
}
