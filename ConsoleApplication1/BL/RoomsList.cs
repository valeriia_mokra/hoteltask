﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    [Serializable]
    public class RoomsList : IMyInterface<HotelRoom>
    {
        private List<HotelRoom> RoomList;

        public HotelRoom this[int index]
        {
            get
            {
                return (HotelRoom)RoomList[index];
            }

            set
            {
                RoomList[index] = value;
            }
        }
        

        public RoomsList(List<HotelRoom> RoomList)
        {
            this.RoomList = RoomList;
        }


        public RoomsList(HotelRoom[] roomArray)
        {
            RoomList.AddRange(roomArray);
        }


        public RoomsList()
        {
            RoomList = new List<HotelRoom>();
        }


        public bool Contains(HotelRoom hotelRoom)
        {
            return RoomList.Contains(hotelRoom);
        }

        public bool Contains(string ID)
        {            
            return RoomList.Exists(x => x.ID == ID);
        }

        public HotelRoom FindRoom(String roomNumber)    ///Поиск по цене
        {
            return RoomList.Find(x => x.ID.Equals(roomNumber));
        }

        public RoomsList FindDate(DateTime from, DateTime to, bool Free)  
        {
            var list = RoomList.Where(x => !(x.Reservations.Where(y => (y.From <= from && y.To > from) ||
            (y.From < to && y.To >= to) || (y.From >= from && y.To <= to)).Count() == 0 ^ Free)).ToList();        

            return new RoomsList(list);
        }
        public RoomsList FindCoast(double costMin, double costMax)    ///Поиск по цене
        {
            return new RoomsList(RoomList.FindAll(x => x.Cost >= costMin && x.Cost <= costMax));
        }

        public RoomsList FindType(RoomTypes type)          //Поиск по типу 
        {
            return new RoomsList(RoomList.FindAll(x =>  x.RoomType.Equals(type)));
        }

        public RoomsList FindFloor(char floor)          //Поиск по этажу 
        {
            return new RoomsList(RoomList.FindAll(x => x.ID[2] == floor));
        }


        public RoomsList FindNumbersWithReservedOnName(string name)          //Поиск по этажу 
        {
            List<HotelRoom> list = RoomList.Where(room =>
            //var list = RoomList.Where(room => room.Reservations.Where(reservation => 
            //           reservation.Name.Equals(name)).Count() > 0).ToList();
            {
                bool result = room.Reservations.Where(reservation =>
                 reservation.Name.Equals(name)).Count() > 0;

                if (result)
                {
                    for(int i = 0; i < room.Reservations.Count;i++)
                    {
                        if (!room.Reservations[i].Name.Equals(name))
                            room.DeleteReservation(i);
                    }
                }
                return result;
            }).ToList();


            return new RoomsList(list);
        }

        public RoomsList FindNumbersWithReservedOnPhone(string phone)          //Поиск по этажу 
        {
            /* var list = RoomList.Where(room => room.Reservations.Where(reservation =>
             reservation.MobileNumber.Equals(phone)).Count() > 0).ToList();*/
            var list = RoomList.Where(room =>
            {
                bool result = room.Reservations.Where(reservation =>
                 reservation.MobileNumber.Equals(phone)).Count() > 0;

                if (result)
                {
                    for (int i = 0; i < room.Reservations.Count; i++)
                    {
                        if (!room.Reservations[i].MobileNumber.Equals(phone))
                            room.DeleteReservation(i);
                    }
                }
                return result;
            }).ToList();

            return new RoomsList(list);
        }

        public override string ToString()
        {
            string _string = "";
            foreach (HotelRoom str in RoomList)
                _string += str.ToString();
            return _string;
        }
        public void AddReservation(Reservation reservation, string roomNumber)           //добавление брони
        {
            int index = RoomList.FindIndex(x => x.ID.Equals(roomNumber));
            RoomList[index].AddReservation(reservation);
            OnAddReservatinEvent(new EventArgs());
        }

        public void Add(HotelRoom item)
        {
            RoomList.Add(item);
        }

        public void Delete(HotelRoom item)
        {
            RoomList.Remove(item);
        }

        public IEnumerator<HotelRoom> GetEnumerator()
        {
            return RoomList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return RoomList.GetEnumerator();
        }

        public event EventHandler<EventArgs> AddReservationEvent;

        private void OnAddReservatinEvent(EventArgs e)
        {
            var handler = AddReservationEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public double GetAllCost(Reservation res, double cost)
        {
            return (res.To - res.From).Days * cost;
        }

        public void CleanOldReservation()
        {
            foreach (HotelRoom room in RoomList)
            {
                if (room.Reservations.Count > 0)
                {
                    for (int i = 0; i < room.Reservations.Count;)// (Reservation reservation ins)
                    {
                        if (room.Reservations[i].To < DateTime.Now)
                        {
                            room.Reservations.RemoveAt(i);
                        }
                        else i++;
                    }
                }
            }
        }

        public RoomsList AllReservations()
        {
            /*
            List<HotelRoom> TempList = new List<HotelRoom>();
            foreach (HotelRoom room in RoomList)
            {
                if (room.Reservations.Count > 0)
                {
                    TempList.Add(room);
                }
            }
            */
            return new RoomsList(RoomList.Where(room => room.Reservations.Count > 0).ToList());
        }

        public void DeleteReservation(string room, int res)
        {
            int index = RoomList.FindIndex(x => x.ID.Equals(room));
            RoomList[index].DeleteReservation(res);
        }

        public void DeleteReservation(string room, Reservation res)
        {
            int index = RoomList.FindIndex(x => x.ID.Equals(room));
            RoomList[index].DeleteReservation(res);
        }

    }
}
