﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    [Serializable]
    public struct HotelRoom
    {
        public string ID { set; get; }
        public double Cost { set; get; }

        public List<Reservation> Reservations;

        
        public RoomTypes RoomType { set; get; }

        public HotelRoom(string ID, double cost, RoomTypes roomType)
        {
            this.ID = ID;
            this.Cost = cost;
            this.RoomType = roomType;
            this.Reservations = new List<Reservation>();
        }

        public void AddReservation(Reservation reservation)
        {
            Reservations.Add(reservation);
        }

        public void DeleteReservation(int reservation)
        {
            Reservations.RemoveAt(reservation);
        }
        public void DeleteReservation(Reservation reservation)
        {
            Reservations.Remove(reservation);
        }

        public override string ToString()
        {
            return String.Format("Room number: {0}; Cost: {1}; Type: {2}\n", ID, Cost, RoomType);
        }

    }
}
