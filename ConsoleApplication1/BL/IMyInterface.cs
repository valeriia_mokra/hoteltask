﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface IMyInterface<HotelRoom> : IEnumerable<HotelRoom>
    {
        HotelRoom this[int index] { get; set; }
        void Add(HotelRoom item);
    }
}
