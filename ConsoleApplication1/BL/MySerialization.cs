﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace BusinessLogic
{
    public class MySerialization<T> : IMySerialization<T>
    {
        private XmlSerializer xmlFormat = new XmlSerializer(typeof(T));

        public void Serialize(T obj)
        {
            using (Stream fStream = new FileStream("Collections.xml", FileMode.Create))
            {
                xmlFormat.Serialize(fStream, obj);
            }
        }

        public T Deserialize()
        {
            using (FileStream fs = new FileStream("Collections.xml", FileMode.Open))
            {
                return (T)xmlFormat.Deserialize(fs);
            }
        }
    }
}
