﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface IMySerialization<T>
    {
        void Serialize(T obj);
        T Deserialize();     
    }
}
